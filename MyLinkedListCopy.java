// Camille Merz
import java.util.concurrent.ThreadLocalRandom;
/**
 * LinkedList class implements a doubly-linked list.
 * Camille Merz
 * 10/4/2018
 * CS 124
 */
public class MyLinkedList<AnyType extends Comparable<? super AnyType>> implements Iterable<AnyType>
{
    /**
     * Construct an empty LinkedList.
     */
    public MyLinkedList( )
    {
        doClear( );
    }
    
    private void clear( )
    {
        doClear( );
    }
    
    /**
     * Change the size of this collection to zero.
     */
    public void doClear( )
    {
        beginMarker = new Node<>( null, null, null );
        endMarker = new Node<>( null, beginMarker, null );
        beginMarker.next = endMarker;
        
        theSize = 0;
        modCount++;
    }
    
    /**
     * Returns the number of items in this collection.
     * @return the number of items in this collection.
     */
    public int size( )
    {
        return theSize;
    }
    
    public boolean isEmpty( )
    {
        return size( ) == 0;
    }
    
    /**
     * Adds an item to this collection, at the end.
     * @param x any object.
     * @return true.
     */
    public boolean add( AnyType x )
    {
        add( size( ), x );   
        return true;         
    }
    
    /**
     * Adds an item to this collection, at specified position.
     * Items at or after that position are slid one position higher.
     * @param x any object.
     * @param idx position to add at.
     * @throws IndexOutOfBoundsException if idx is not between 0 and size(), inclusive.
     */
    public void add( int idx, AnyType x )
    {
        addBefore( getNode( idx, 0, size( ) ), x );
    }
    
    /**
     * Adds an item to this collection, at specified position p.
     * Items at or after that position are slid one position higher.
     * @param p Node to add before.
     * @param x any object.
     * @throws IndexOutOfBoundsException if idx is not between 0 and size(), inclusive.
     */    
    private void addBefore( Node<AnyType> p, AnyType x )
    {
        Node<AnyType> newNode = new Node<>( x, p.prev, p );
        newNode.prev.next = newNode;
        p.prev = newNode;         
        theSize++;
        modCount++;
    }   
    
    
    /**
     * Returns the item at position idx.
     * @param idx the index to search in.
     * @throws IndexOutOfBoundsException if index is out of range.
     */
    public AnyType get( int idx )
    {
        return getNode( idx ).data;
    }
        
    /**
     * Changes the item at position idx.
     * @param idx the index to change.
     * @param newVal the new value.
     * @return the old value.
     * @throws IndexOutOfBoundsException if index is out of range.
     */
    public AnyType set( int idx, AnyType newVal )
    {
        Node<AnyType> p = getNode( idx );
        AnyType oldVal = p.data;
        
        p.data = newVal;   
        return oldVal;
    }
    
    /**
     * Gets the Node at position idx, which must range from 0 to size( ) - 1.
     * @param idx index to search at.
     * @return internal node corresponding to idx.
     * @throws IndexOutOfBoundsException if idx is not between 0 and size( ) - 1, inclusive.
     */
    private Node<AnyType> getNode( int idx )
    {
        return getNode( idx, 0, size( ) - 1 );
    }

    /**
     * Gets the Node at position idx, which must range from lower to upper.
     * @param idx index to search at.
     * @param lower lowest valid index.
     * @param upper highest valid index.
     * @return internal node corresponding to idx.
     * @throws IndexOutOfBoundsException if idx is not between lower and upper, inclusive.
     */    
    private Node<AnyType> getNode( int idx, int lower, int upper )
    {
        Node<AnyType> p;
        
        if( idx < lower || idx > upper )
            throw new IndexOutOfBoundsException( "getNode index: " + idx + "; size: " + size( ) );
            
        if( idx < size( ) / 2 )
        {
            p = beginMarker.next;
            for( int i = 0; i < idx; i++ )
                p = p.next;            
        }
        else
        {
            p = endMarker;
            for( int i = size( ); i > idx; i-- )
                p = p.prev;
        } 
        
        return p;
    }
    
    /**
     * Removes an item from this collection.
     * @param idx the index of the object.
     * @return the item was removed from the collection.
     */
    public AnyType remove( int idx )
    {
        return remove( getNode( idx ) );
    }
    
    /**
     * Removes the object contained in Node p.
     * @param p the Node containing the object.
     * @return the item was removed from the collection.
     */
    private AnyType remove( Node<AnyType> p )
    {
        p.next.prev = p.prev;
        p.prev.next = p.next;
        theSize--;
        modCount++;
        
        return p.data;
    }
    
    /**
     * Returns a String representation of this collection.
     */
    public String toString( )
    {
        StringBuilder sb = new StringBuilder( "[ " );

        for( AnyType x : this )
            sb.append( x + " " );
        sb.append( "]" );

        return new String( sb );
    }

    /**
     * Obtains an Iterator object used to traverse the collection.
     * @return an iterator positioned prior to the first element.
     */
    public java.util.Iterator<AnyType> iterator( )
    {
        return new LinkedListIterator( );
    }

    /**
     * Max methods allows data retrieval for findMax()
     */
    public AnyType max() {
        return ( isEmpty()? null : findMax().data);
    }

    /**
     * Returns reference to node with max value
     *
     */
    private Node <AnyType> findMax( ) {
        AnyType max = null;

        //Create iterator
        java.util.Iterator<AnyType> maxIt = this.iterator();

        //Move through list and compare values to find max
        while(maxIt.hasNext())
        {
            AnyType next = maxIt.next();
            if(max == null || next.compareTo(max) > 0)
               max = next;
        }
        return new Node <AnyType>(max, null,null);
    }



    /**
     * This is the implementation of the LinkedListIterator.
     * It maintains a notion of a current position and of
     * course the implicit reference to the MyLinkedList.
     */
    private class LinkedListIterator implements java.util.Iterator<AnyType>
    {
        private Node<AnyType> current = beginMarker.next;
        private int expectedModCount = modCount;
        private boolean okToRemove = false;
        
        public boolean hasNext( )
        {
            return current != endMarker;
        }
        
        public AnyType next( )
        {
            if( modCount != expectedModCount )
                throw new java.util.ConcurrentModificationException( );
            if( !hasNext( ) )
                throw new java.util.NoSuchElementException( ); 
                   
            AnyType nextItem = current.data;
            current = current.next;
            okToRemove = true;
            return nextItem;
        }
        
        public void remove( )
        {
            if( modCount != expectedModCount )
                throw new java.util.ConcurrentModificationException( );
            if( !okToRemove )
                throw new IllegalStateException( );
                
            MyLinkedList.this.remove( current.prev );
            expectedModCount++;
            okToRemove = false;       
        }
    }
    
    /**
     * This is the doubly-linked list node.
     */
    private static class Node<AnyType>
    {
        public Node( AnyType d, Node<AnyType> p, Node<AnyType> n )
        {
            data = d; prev = p; next = n;
        }
        
        public AnyType data;
        public Node<AnyType>   prev;
        public Node<AnyType>   next;
    }
    
    private int theSize;
    private int modCount = 0;
    private Node<AnyType> beginMarker;
    private Node<AnyType> endMarker;
}

class TestLinkedList
{
    public static void main( String [ ] args )
    {
        MyLinkedList<Integer> lst = new MyLinkedList<>( );

        for( int i = 0; i < 10; i++ )
                lst.add( i );
        for( int i = 20; i < 30; i++ )
                lst.add( 0, i );

        lst.remove( 0 );
        lst.remove( lst.size( ) - 1 );

        System.out.println( lst );

        java.util.Iterator<Integer> itr = lst.iterator( );
        while( itr.hasNext( ) )
        {
                itr.next( );
                itr.remove( );
                System.out.println( lst );
        }

        //Create new MyLinkedList<Integer>
        MyLinkedList<Integer> myList = new MyLinkedList<>();
        //Insert integers multiples of 3 from 6 - 45 into myList
        for(int i = 6; i <= 45; i=i+3) {
            myList.add(i);
        }

        //Print myList
        System.out.println("Original List");
        System.out.println(myList);

        //Create iterator
        java.util.Iterator<Integer> listIt = myList.iterator();

        //Remove all even numbers and multiples of 9 from myList
        while(listIt.hasNext())
        {
            int next = listIt.next();
            if((next % 2 == 0) || (next % 9 == 0))
            {
                listIt.remove();
            }
        }

        //print myList
        System.out.println("Without evens and multiples of 9");
        System.out.println(myList);

        //Create iterator
        java.util.Iterator<Integer> listIt2 = myList.iterator();
        //remove all odd numbers from myList
        while(listIt2.hasNext())
        {
            int next = listIt2.next();
            if((next % 2 != 0))
            {
                listIt2.remove();
            }
        }

        //print myList
        System.out.println("Without odds");
        System.out.println(myList);

        System.out.println("Homework 5 - I");
        System.out.println("A: Original List (Empty)");
        MyLinkedList<Integer> myList2 = new MyLinkedList<>();
        System.out.println(myList2);
        System.out.println("Max: ");
        System.out.println(myList2.max());

        System.out.println("B: Original List (Sorted List of Size 10)");
        for(int i = 1; i <= 10; ++i) {
            myList2.add(i);
        }
        System.out.println(myList2);
        System.out.println("Max: ");
        System.out.println(myList2.max());

        System.out.println("C: Original List (Reverse Sorted List of Size 10)");
        MyLinkedList<Integer> myList3 = new MyLinkedList<>();
        for(int i = 10; i >= 1; --i) {
            myList3.add(i);
        }
        System.out.println(myList3);
        System.out.println("Max: ");
        System.out.println(myList3.max());

        System.out.println("D: Original List (Random List of Size 10)");
        MyLinkedList<Integer> myList4 = new MyLinkedList<>();
        int randomNum;
        for(int i = 10; i >= 1; --i) {
            myList4.add(randomNum = ThreadLocalRandom.current().nextInt(0, 30 + 1));
        }
        System.out.println(myList4);
        System.out.println("Max: ");
        System.out.println(myList4.max());

        //Runtime is O(N)
    }
}
